function[PathName,FileName] = uigetMultiFilesandFolders(start_path,ext,file_or_dir)
% This function was adapted from code found on Stack Overflow:
% https://stackoverflow.com/questions/6349410/using-uigetfile-instead-of-uigetdir-to-get-directories-in-matlab/6349803
% and is based on the answers of the users
% Andrew Janke (https://stackoverflow.com/users/105904/andrew-janke),
% Peugas (https://stackoverflow.com/users/898946/peugas) and 
% Nils (https://stackoverflow.com/users/6847446/nils)
% on a question asked by Paul (https://stackoverflow.com/users/467366/paul)

import com.mathworks.mwswing.MJFileChooserPerPlatform;

if nargin == 0 || ~isdir(start_path)
    start_path = pwd;
end

jchooser = javaObjectEDT('com.mathworks.mwswing.MJFileChooserPerPlatform',start_path);

if nargin < 3
    jchooser.setFileSelectionMode(javax.swing.JFileChooser.FILES_AND_DIRECTORIES);
elseif strcmpi(file_or_dir,'both') || (~strcmpi(file_or_dir,'dir') && ~strcmpi(file_or_dir,'file'))
    jchooser.setFileSelectionMode(javax.swing.JFileChooser.FILES_AND_DIRECTORIES);
elseif strcmpi(file_or_dir,'dir')
    jchooser.setFileSelectionMode(javax.swing.JFileChooser.DIRECTORIES_ONLY);
elseif strcmpi(file_or_dir,'file')
    jchooser.setFileSelectionMode(javax.swing.JFileChooser.FILES_ONLY);   
end
     
if nargin > 1
    if ~isempty(ext)   
        if ischar(ext); ext = {ext};    end
        [~,f,exts] = cellfun(@fileparts,ext,'UniformOutput',false);
        usef = cellfun(@isempty,exts);
        exts(usef) = cellfun(@(x) ['.' x],f(usef),'UniformOutput',false);
        exts = cellfun(@(x) ['*',x],exts,'UniformOutput',false);
        fileFilter = com.mathworks.mwswing.FileExtensionFilter('user_specified2', exts, false, true);
        javaMethodEDT('addChoosableFileFilter',jchooser,fileFilter);
    end
end

jchooser.setMultiSelectionEnabled(true);

jchooser.showOpenDialog([]);

if jchooser.getState() == javax.swing.JFileChooser.APPROVE_OPTION
    jFiles = jchooser.getSelectedFiles();
    files = arrayfun(@(x) char(x.getPath()), jFiles, 'UniformOutput', false);
elseif jchooser.getState() == javax.swing.JFileChooser.CANCEL_OPTION
    files = [];
else
    error('Error occurred while picking file');
end

[PathName,FileName,extensions] = cellfun(@fileparts,files,'UniformOutput',false);
isadir= cellfun(@isempty,extensions);
PathName = cellfun(@(x) [x filesep],PathName,'UniformOutput',false);
FileName(~isadir) = cellfun(@(x,y) [x,y],FileName(~isadir),extensions(~isadir),'UniformOutput',false);
FileName(isadir)  = {''};


