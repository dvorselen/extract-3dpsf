function[allPSFs,XYZ,dims,Int] = Extract_depthvar_3DPSF(varargin)
    
% This function allows extracting a 3D PSF from images with multiple
% emitters that can be used for deconvolution. It works best with sparsely
% present emitters, and mostly single particles. It will find particles on
% multiple images, do background subtraction and determine their distance to 
% the surface, such that later you can average the PSFs within the desired 
% range of distances from the surface.
%
% INPUT:
% The desired dimensions of your PSF (should be tuned to your imaging setup
% (objective/confocal vs epi/medium), default settings are pretty good for 
% 63x confocal in PBS.
% 'PSF_zdim_um'   : desired size of PSF in Z        (default: 10 um)
% 'PSF_xydim_um'  : desired size of PSF in X,Y      (default: 5 um)
% 'smooth'        : smoothing of input images in pixels. This is only done
%                   to simplify the analysis, and not actually used for
%                   determining the PSF, since this would broaden the PSF
%                                                   (default: 0.5 pixels)
% 'autoflip'      : If autoflip is true, the code potentially FLIPs your psf. 
%                   It does this based on the broadening for PSFs farther 
%                   from the surface. The convention I use is that the plane 
%                   indexed by 1 is furthest from the sample surface.
%                                                   (default: 0, no flipping)
% 'zheightmethod' : Determines how the position of each PSF with respect to
%                   the glass is determined. Either compared to the lowest
%                   particle ('firstparticle') or with respect to the
%                   maximum intensity plane ('maxplane'). (default: firstparticle)
%
% OUTPUT:
% allPSFs: all the 3D PSFs (backgrounds subtracted)
% XYZ    : The location of each PSF (Z is the distance from the surface)
% dims   : the actual dimensions of the PSF image (I do not change the pixel
%          size, so it can differ slightly from the desired size given as input)
% Int    : The total intensity of the PSF
%

    % Update default values if user specified input
    DefStruct = struct( 'PSF_zdim_um', 10 , 'PSF_xydim_um', 5, 'smooth', 0.5,...
        'autoflip', 0, 'zheightmethod', 'firstparticle','centroidestimate','box');
    Args = parseArgs( varargin, DefStruct, [] );
    PSF_xydim_um = Args.PSF_xydim_um;
    PSF_zdim_um  = Args.PSF_zdim_um;

    if strcmpi(Args.centroidestimate,'gaussian')
        % Create fitting function and set options
        gausFun = @(hms,x) hms(1) .* exp (-(x-hms(2)).^2 ./ (2*hms(3)^2));
        options = optimset('Display','off','Algorithm','levenberg-marquardt');
    end

    % Let the user select images from which to extract the PSF
    [PathName,FileName] = uigetMultiFilesandFolders(fullfile(...    
        '/Users/daanvorselen/Documents/Projects/Macrophage phagocytosis/Raw Data'),'*.tif','file'); 

    % Read the images
    if iscell(FileName)
        % In case there are multiple, we go through a loop
        Nfiles = length(FileName);
        data = cell(Nfiles,4);
        for ifile = 1:Nfiles 
            data(ifile,:) = bfopen([PathName{ifile} FileName{ifile}]);
        end
    else
        % the single image case
        Nfiles = 1;
        data = {bfopen([PathName FileName])};
    end

    % Find the number of slices of the files
    Nslices = cellfun(@(x) size(x,1),data(:,1));
    if ~isempty(data{1,4}.getPixelsPhysicalSizeX(0))
        try
            PixelSizeXY = cellfun(@(x) double(x.getPixelsPhysicalSizeX(0).value(ome.units.UNITS.MICROMETER)),data(:,4));
        catch
            PixelSizeXY = cellfun(@(x) double(x.getPixelsPhysicalSizeX(0).value(ome.units.UNITS.MICROM)),data(:,4));
        end
    else
        warning('PixelSize (x,y) missing from file')
        uipixelsize = str2double(inputdlg('What is the x,y-pixelsize in this experiment','PixelSize?',1,{'0.1425'}));
        PixelSizeXY = ones(size(Nslices))*uipixelsize;
    end
    if ~isempty(data{1,4}.getPixelsPhysicalSizeZ(0))
        try
            PixelSizeZ = cellfun(@(x) double(x.getPixelsPhysicalSizeZ(0).value(ome.units.UNITS.MICROMETER)),data(:,4));
        catch
            PixelSizeZ = cellfun(@(x) double(x.getPixelsPhysicalSizeZ(0).value(ome.units.UNITS.MICROM)),data(:,4));
        end
        uipixelsize = str2double(inputdlg(['Detected z-pixelsize is ' num2str(PixelSizeZ(1)) '.' newline 'If this is incorrect,'...
            ' enter correct value below'],'PixelSize?',1,{num2str(PixelSizeZ(1))}));
    else
        warning('PixelSize (z) missing from file')
        uipixelsize = str2double(inputdlg('What is the z-pixelsize in this experiment','PixelSize?',1,{'0.2'}));
    end
    PixelSizeZ  = ones(size(Nslices))*uipixelsize;

    IMs = cell(1,Nfiles);

    % Determine the direction of the z-stack (up to down or down to up)
    for i = 1:Nfiles
        IMs{i} = cat(3,data{i,1}{1:Nslices(i),1});
    end

    clear('data')

    % Smooth the images 
    if Args.smooth > 0 
        IMs_smoothed = cellfun(@(x) imgaussfilt3(x,Args.smooth),IMs,'UniformOutput',false);
    else
        IMs_smoothed = IMs;
    end

    % Find maximum intensity frames
    DisttoEdge = zeros(1,Nfiles);
    BackGroundPlanes = cell(1,Nfiles);
    maxPlane = zeros(1,Nfiles);

    % Find background slices
    for i = 1:Nfiles
        dims = size(IMs_smoothed{i});
        plane_int = prctile(reshape(IMs_smoothed{i},[dims(1)*dims(2) dims(3)]),99.99);
        [~,maxPlane(i)] = max(plane_int);
        % Determine background slices based on max. intensity in the plane
        backgroundplanes = plane_int < min(plane_int)*1.5;
        bottombackgroundplanes = find(backgroundplanes==0,1,'first');
        topbackgroundplanes = find(backgroundplanes==0,1,'last');
        backgroundplanes(bottombackgroundplanes:topbackgroundplanes) = 0;
        BackGroundPlanes{i} = backgroundplanes;
        % Seperately determine particle planes, we use these to determine if
        % the particles are too close to the image edge
        particleplanes = plane_int > min(plane_int)*5;
        particleplanes (find(particleplanes,1,'first'):end) = 1;
        if particleplanes(1)
            DisttoEdge(i) = find(particleplanes(end:-1:1),1,'first');
        elseif particleplanes(end)
            DisttoEdge(i) = find(particleplanes,1,'first');
        else
            DisttoEdge(i) = min(find(particleplanes,1,'first'),find(particleplanes(end:-1:1),1,'first'));
        end
    end

    % Throw out images where the first frame with particles is too close to the
    % end of the Z-stack. This is mostly important when the first particle is
    % used to determine the z-position of the others.
    if strcmp('zheightmethod','firstparticle')
        ExcludeIM = any(DisttoEdge' < PSF_zdim_um./PixelSizeZ*0.5,2);
        % Update all the affected variables
        IMs(ExcludeIM) = [];
        IMs_smoothed(ExcludeIM) = [];
        Nfiles = Nfiles - sum(ExcludeIM);
        Nslices(ExcludeIM) = [];
        PixelSizeXY(ExcludeIM) = [];
        PixelSizeZ(ExcludeIM) = [];

        % Make the user aware that some images are excluded
        if all(ExcludeIM)
            error(['The maximum intensity frame(s) is/are too close to the z-limits, which would cut off the PSF '...
                'and make it inappopriate for deconvolution'])
        elseif any(ExcludeIM)
            for imessage = find(ExcludeIM)'
                warning(['Image ' FileName{imessage} ' not used, particles were too close to the z-limits'])
            end
        end
    end

    % Subtract background from the images
    IMs_proc = cell(1,Nfiles);

    for i = 1:Nfiles
        backgroundplanes = BackGroundPlanes{i};

        backgroundismooth = imgaussfilt(mean(IMs_smoothed{i}(:,:,backgroundplanes),3),ceil(0.5/PixelSizeXY(i)));
        IMs_proc{i} = double(IMs_smoothed{i}) - repmat(backgroundismooth,[1 1 Nslices(i)]);

        backgroundi = mean(IMs{i}(:,:,backgroundplanes),3);
        IMs{i} = double(IMs{i}) - repmat(backgroundi,[1 1 Nslices(i)]);
    end

    % Find local maxima in the planes containing particles
    IMstats = struct('WeightedCentroid',[],'MaxIntensity',[],'IMnumber',[],'PrincipalAxisLength',[]);
    CCs = struct('Connectivity',cell(1,Nfiles),'ImageSize',cell(1,Nfiles),...
        'NumObjects',cell(1,Nfiles),'PixelIdxList',cell(1,Nfiles));

    for i = 1:Nfiles
        currplanes = IMs_proc{i}(:,:,~BackGroundPlanes{i});
        % Find particles based on a threshold. We set the threshold at 0.05 %
        % highest pixels with a minimum of 5x the standard deviation over the background
        threshold = max([prctile(currplanes(:),99.95) 5*std(currplanes(:))]);
        BW = currplanes>threshold;
        % Remove particles consisting of less than 10 pixels
        BW = bwareaopen(BW,10);

        % Calculate some properties of the areas
        CCs(i) = bwconncomp(BW);
        stats = regionprops3(CCs(i),currplanes,'WeightedCentroid','MaxIntensity','PrincipalAxisLength');
        % Correct the weighted centroid for the backgroundplanes that were taken out
        stats.WeightedCentroid(:,3) = stats.WeightedCentroid(:,3)+find(BackGroundPlanes{i}==0,1,'first');

        if Args.autoflip
            % Determine the orientation of the stack, making use of the widening of
            % the PSFs along the z-direction further from the surface
            linfitparms = robustfit(stats.WeightedCentroid(:,3),stats.PrincipalAxisLength(:,1));
            if linfitparms(2) > 0 
                CCs(i)                      = bwconncomp(BW(:,:,end:-1:1));
                IMs{i}                      = IMs{i}(:,:,end:-1:1);
                stats.WeightedCentroid(:,3) = size(IMs{i},3)-stats.WeightedCentroid(:,3);
            end
        end

        % Find particles that are close together
        centroidlocs_inum = stats.WeightedCentroid.*[PixelSizeXY(i) PixelSizeXY(i) PixelSizeZ(i)];
        centroidlocs_norm = centroidlocs_inum./[PSF_xydim_um PSF_xydim_um PSF_zdim_um];
        DistEU = pdist2(centroidlocs_norm,centroidlocs_norm);
        DistEU(DistEU == 0) = Inf;
        freeparticle = all(DistEU>1);

        % or close to the border of the image
        leftborder = any(stats.WeightedCentroid(:,1:2)<PSF_xydim_um./PixelSizeXY(i),2);
        rightborder = any(size(currplanes,[2 1])-stats.WeightedCentroid(:,1:2)<PSF_xydim_um./PixelSizeXY(i),2);
        tooclosetoborder = leftborder | rightborder;

        % Exclude them both
        include = freeparticle & ~tooclosetoborder';
        CCs(i).PixelIdxList = CCs(i).PixelIdxList(include);
        CCs(i).NumObjects = sum(include);
        stats(~include,:) = [];
        stats.IMnumber    = repmat(i,sum(include),1);

        % Save remaining data in IMstats
        if i == 1
            IMstats(1:sum(include)) = table2struct(stats);
        else
            IMstats(end+1:end+sum(include)) = table2struct(stats);
        end
    end

    % Show the maximum intensity histogram and a suggested 
    figure('Name','Maximum Intensity Histogram')
    emitter_maxima = double([IMstats.MaxIntensity]);
    histogram(emitter_maxima,logspace(log10(min(emitter_maxima)),...
        log10(max(emitter_maxima)),max(5,round(length(emitter_maxima)/20))));
    set(gca,'xscale','log')
    hold on
    ylims = get(gca,'ylim');
    Int_emitter_estimate_mean = mean(double([IMstats.MaxIntensity]));
    Int_emitter_estimate_high = min([round(exp(log(Int_emitter_estimate_mean)*1.2),2,'significant') 6e4]);
    Int_emitter_estimate_low = max(round(exp(log(Int_emitter_estimate_mean)/1.2),2,'significant'));
    plot([Int_emitter_estimate_high Int_emitter_estimate_high],ylims,'-r')
    plot([Int_emitter_estimate_low Int_emitter_estimate_low],ylims,'-r')
    hold off

    % Ask the user to confirm intensity cutoff
    dlgoptions.WindowStyle = 'normal';
    int_emitter_str = inputdlg({'Enter maximum emitter intensity','Enter minimum emitter intensity'},...
        'Provide emitter intensity',2,{num2str(Int_emitter_estimate_high),num2str(Int_emitter_estimate_low)},dlgoptions);
    disp(['Used ' int_emitter_str{1} ' as upper intensity cutoff for particles to include'])
    disp(['Used ' int_emitter_str{2} ' as lower intensity cutoff for particles to include'])
    max_int_emitter = str2double(int_emitter_str{1});
    min_int_emitter = str2double(int_emitter_str{2});

    % Check user input
    if min_int_emitter > max_int_emitter
        warning(['Minimum intensity was set higher than the maximum intensity: '...
            'minimum and maximum were swapped'])
        max_int_emitter = min_int_emitter;
        min_int_emitter = str2double(int_emitter_str{1});
    end

    % Select the particles within the user defined limits
    selected_emitters  = [IMstats.MaxIntensity] < max_int_emitter & [IMstats.MaxIntensity] > min_int_emitter;
    IMstats_single     = IMstats(selected_emitters);

    % Setup output var for looping through the PSFs
    Norm_single_emitter = cell(1,sum(selected_emitters));
    Int = zeros(1,sum(selected_emitters));

    % Cut out the images of the single emitters and normalize
    for ise = 1:sum(selected_emitters)

        % Find the corresponding image and the centroid of this particle
        IMi          = IMstats_single(ise).IMnumber;
        IMi_centroid = IMstats_single(ise).WeightedCentroid;

        % Draw a box around the centroid of the particle
        PSF_xydim_px = PSF_xydim_um./PixelSizeXY(IMi)/2;
        PSF_zdim_px  = PSF_zdim_um ./PixelSizeZ(IMi) /2;

        xrange = round( IMi_centroid(2)-PSF_xydim_px*1.25:IMi_centroid(2)+PSF_xydim_px*1.25 );
        yrange = round( IMi_centroid(1)-PSF_xydim_px*1.25:IMi_centroid(1)+PSF_xydim_px*1.25 );
        zrange = round( IMi_centroid(3)-PSF_zdim_px*1.25 :IMi_centroid(3)+PSF_zdim_px*1.25  );

        % Check that we are within the image boundaries
        if zrange(1)<0 || zrange(end)>size(IMs{IMi},3);         continue;       end

        % Update the centroid estimate, to align the various particles precisely. Now we switch back to the unsmoothed image
        zoomIM = IMs{IMi}(xrange,yrange,zrange);

        if strcmpi(Args.centroidestimate,'gaussian')

        selectZOOM = zoomIM(round(PSF_xydim_px*.6):end-round(PSF_xydim_px*.6),...
            round(PSF_xydim_px*.6):end-round(PSF_xydim_px*.6),...
            round(PSF_zdim_px*.5):end-round(PSF_zdim_px*.5));   

            try
                lineprofile = sum(sum(selectZOOM,3),1);
                x = (0:length(lineprofile)-1) + round(PSF_xydim_px*.6);
                weighted_centre(1) = fitPSF;

                lineprofile = sum(sum(selectZOOM,3),2)';
                weighted_centre(2) = fitPSF;

                lineprofile = max(squeeze(sum(selectZOOM,2)),[],1);
                x = (0:length(lineprofile)-1) + round(PSF_zdim_px*.5);
                weighted_centre(3) = fitPSF;

            catch  
                % if something goes wrong with the fitting, we throw out the data
                weighted_centre = [-1 -1 -1];
            end

        else

            selectMAT = zeros(size(zoomIM)); % cut small box around the particle to minimize effect of any background gradients
            selectMAT(round(PSF_xydim_px*.4):end-round(PSF_xydim_px*.4),...
                round(PSF_xydim_px*.4):end-round(PSF_xydim_px*.4),...
                round(PSF_zdim_px*.4):end-round(PSF_zdim_px*.4)) = 1;   
            stat = regionprops(selectMAT,zoomIM,'WeightedCentroid');
            weighted_centre = stat.WeightedCentroid;

        end

        % Cut the final box around particles
        xrangeu = weighted_centre(1)-PSF_xydim_px:weighted_centre(1)+PSF_xydim_px-1;
        yrangeu = weighted_centre(2)-PSF_xydim_px:weighted_centre(2)+PSF_xydim_px-1;
        zrangeu = weighted_centre(3)-PSF_zdim_px :weighted_centre(3)+PSF_zdim_px-1 ;    

        % If the first estimate of the centroid was far off, bugs can
        % occur, so we skip these particles
        if any([xrangeu, yrangeu, zrangeu]<1) ...
                || any([xrangeu(end) yrangeu(end) zrangeu(end)]>[size(zoomIM,1) size(zoomIM,2) size(zoomIM,3)])
            continue
        end

        % Use interpolation to align the PSFs with subpixel accuracy
        [X,Y,Z]    = meshgrid(xrange-xrange(1),yrange-yrange(1),zrange-zrange(1));
        [Xu,Yu,Zu] = meshgrid(xrangeu,yrangeu,zrangeu);

        % Normalize to 1 and store the particle
        single_emitter_i = interp3(X,Y,Z,zoomIM,Xu,Yu,Zu,'linear');
        Int(ise) = sum(single_emitter_i(:));
        Norm_single_emitter{ise} = single_emitter_i/sum(single_emitter_i(:));

    end

    included = ~cellfun(@isempty,Norm_single_emitter);
    allPSFs = Norm_single_emitter(included);
    Int = Int(included);
    XYZ = vertcat(IMstats_single(included).WeightedCentroid); 

    belongstoIM = vertcat(IMstats(included).IMnumber);
    Z = XYZ(:,3).*PixelSizeZ(belongstoIM);
    XY = XYZ(:,[1 2]).*PixelSizeXY(belongstoIM);
    
    % Change the Z-height of all particles such that the first particle is assumed to be on the glass surface
    if strcmp('zheightmethod','firstparticle')
        for i = 1:Nfiles
            Zi = Z(belongstoIM==i);
            Z(belongstoIM==i) = -(Zi - max(Zi));
        end
    else 
        for i = 1:Nfiles
            Zi = Z(belongstoIM==i);
            Z(belongstoIM==i) = -Zi+(find(~BackGroundPlanes{i},1,'last')-maxPlane(i))*PixelSizeZ(i);
        end
    end
    XYZ = [XY Z];
    
    % Also return the exact dimensions of the corresponding figure
    dims = [PixelSizeXY(1) PixelSizeXY(1) PixelSizeZ(1)].*size(allPSFs{1});

    % Remove any images with NaN pixel values
    containsNaN = cellfun(@(x) any(isnan(x(:))),allPSFs);
    allPSFs(containsNaN) = [];
    XYZ(containsNaN,:)   = [];



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function peakloc = fitPSF

        [pks,locs,w] = findpeaks(lineprofile, 'NPeaks', 1,'Sortstr','descend');
        include_in_fit = false(1,length(x));
        include_in_fit(ceil(locs-w):floor(locs+w)) = 1;
        fitted_hms = lsqcurvefit(gausFun,[pks,locs+x(1),w/2.35],x(include_in_fit),lineprofile(include_in_fit),[],[],options);    
        peakloc = fitted_hms(2);

    end   

end