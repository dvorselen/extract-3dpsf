%% Depth dependent PSF guide

%% Select images and obtain PSFs

PSFdims = str2double(inputdlg({'XY','Z'},'Enter the desired PSF dims',[1 100],{'5','10'}));
[allPSFs,XYZ,dims,Int] = Extract_depthvar_3DPSF('PSF_xydim_um',PSFdims(1),'PSF_zdim_um',PSFdims(2),'zheightmethod','maxplane');

% The code can automatically flip your psf. It does this based on the broadening for PSFs farther 
% from the surface. The convention I use is that the plane indexed by 1 is furthest from the sample surface.
% If you want to make use of this feature, use: [allPSFs,XYZ,dims] = Extract_depthvar_3DPSF('autoflip',1);

%% Select PSFs within a certain range from the surface (in um) and average

% The default settings will select all PSFs between 0 and 10 um depth
bounds = str2double(inputdlg({'lowerbound','upperbound'},'Enter the bounds of your Z-range of interest',[1 70],{'0','10'}));

selected = XYZ(:,3)>bounds(1) & XYZ(:,3)<bounds(2); 
PSF3D = mean(cat(4,allPSFs{selected}),4);

%% Save PSF as tiff file and as .mat file for reading back into matlab

% convert to int16 for saving as .tif file
PSF3D = double(PSF3D);
PSF3Dint16 = uint16(PSF3D*60000/double(max(PSF3D(:))));
disp(['The summed value of your PSF is ' num2str(sum(PSF3Dint16(:)))])
disp(['The number of PSFs used is ' num2str(sum(selected))])

% filename
fname = [cd filesep char(datetime('today','Format','yyyyMMdd')) '_PSF3D_'...
    num2str(bounds(1)) '-' num2str(bounds(2)) 'um'];

% Write the first plane
imwrite(PSF3Dint16(:,:,1),[fname '.tif'],'tif')

% Write the others by appending
for i = 2:size(PSF3Dint16,3)
    imwrite(PSF3Dint16(:,:,i), [fname '.tif'],'writemode', 'append');
end

save([fname '.mat'],'PSF3D')

%% Save matlab workspace containing the PSFs, dimensions and location

filename = 'PSF3D.mat';

save([char(datetime('today','Format','yyyyMMdd')) '_' filename],'allPSFs','dims','XYZ','Int','-v7.3')

