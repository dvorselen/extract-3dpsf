Matlab code for extraction of a (3D) PSF from confocal imaging stacks. It is especially useful to obtain PSFs measured at different distances from the coverslip. PSFs can dramatically change depending on imaging depth, and using a PSF measured at a similar depth as your sample can significantly improve deconvolution results. To measure a PSF away from the sample surface, tracer particles can be embedded in a (polyacrylamide) gel (also see the reference below). The code can also be used for tracer particles stuck to a glass surface.

If you find the code particularly useful, please consider citing the following work: 
Vorselen, D. et al. Microparticle traction force microscopy reveals subcellular force exertion patterns in immune cell–target interactions. Nat. Commun. 11, 20 (2020).

-------------------------

The main script to run is Depth_dependent_PSF_guide. This script contain “code sections”, which are used to guide you one-by-one through the PSF extraction. Particularly, these are the steps to obtain a PSF*.
- Run the cell "Select images and obtain PSFs"
- Enter the desired values for total PSF dimensions in the pop-up box**
- Run the other cells of this script one-by-one

*Using too small of an image for a PSF reconstruction can lead to boundary effects and less accurate devoncolution. On the other hand, using a larger image than necessary can significantly slow down deconvolution. I would recommend using the calculator on https://svi.nl/NyquistCalculator to make sure that you make a PSF image of near-optimal size.
**For Theriot lab members see the folder on our lab archive for additional details/reccomendations

-------------------------

Somehow, I struggle to save a tiff of a normalized PSF in Matlab. If you need to use the PSF in other software (e.g. imageJ), I would reccomend the following steps for normalization (to 1): 
- Open the PSF saved by matlab (as tiff) in imageJ and go to process -> math -> divide
- Enter the number displayed in matlab (“the summed value of your PSF is” …)
- Save the normalized PSF from imageJ

